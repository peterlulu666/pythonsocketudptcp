# We will import the socket module
import socket

# We will set the server name to localhost
server_name = "localhost"
# We will arbitrarily choose 13000 for the server port number
server_port = 13000
# We will creates the client’s socket
# AF_INET indicates that it is using IPv4
# SOCK_DGRAM indicates that the socket is the UDP
client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# We want the user enter the lowercase
send_info = input("Enter lowercase: ")
# We will want to send the info through the socket to the destination host
encode_info_to_bytes = send_info.encode()
client_socket.sendto(encode_info_to_bytes, (server_name, server_port))
# When a packet arrives from the Internet at the client’s socket,
# the packet’s data is put into the variable receive_info and
# the packet’s source address is put into the variable server_address
receive_info, server_address = client_socket.recvfrom(2048)
# We will print the receive_info on the user’s display
decode_info_to_string = receive_info.decode()
print(decode_info_to_string)
# We will closes the socket
client_socket.close()
