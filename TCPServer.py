# We will import the socket module
import socket

# It is the same port number as in the client
server_port = 13000
# We will creates the server’s socket
# AF_INET indicates that it is using IPv4
# SOCK_STREAM indicates that the socket is the TCP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# We will bind the port number 13000 to the server’s socket
server_socket.bind(("", server_port))
# The server_socket is the welcoming socket
# After establish- ing this welcoming door,
# we will wait and listen for some client to knock on the door
# The server will listen for TCP connection requests from the client
server_socket.listen(1)
# We will let the user know that the server is running
print("The server is ready to receive ")
while True:
    # When a client knocks on this door,
    # it will use the accept() to create a new socket in the server
    connection_socket, address = server_socket.accept()
    # The client and server will complete the handshaking,
    # creating a TCP connection between the client’s client_socket and
    # the server’s connection_socket
    # If we have the TCP connection, we will receive data from client
    receive_info = connection_socket.recv(1024)
    # We will uses the method upper() to capitalize the receive_info
    decode_info_to_string = receive_info.decode()
    uppercase_info = decode_info_to_string.upper()
    # We will send the uppercase_info through the server's socket and into the TCP connection
    encode_info_to_bytes = uppercase_info.encode()
    connection_socket.send(encode_info_to_bytes)
    # We will closes the socket
    connection_socket.close()
