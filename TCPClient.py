# We will import the socket module
import socket

# We will set the server name to localhost
server_name = "localhost"
# We will arbitrarily choose 13000 for the server port number
server_port = 13000
# We will creates the client’s socket
# AF_INET indicates that it is using IPv4
# SOCK_STREAM indicates that the socket is the TCP
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# The TCP connection must first be established between the client and server
# So we will perform the three way handshake and
# initiate the TCP connection between the client and server
client_socket.connect((server_name, server_port))
# We want the user enter the lowercase
send_info = input("Enter lowercase: ")
# We will send the info through the client’s socket and into the TCP connection
encode_info_to_bytes = send_info.encode()
client_socket.send(encode_info_to_bytes)
# The client will wait data from server
# We will store the data into receive_info
receive_info = client_socket.recv(1024)
# We will print the receive_info on the user’s display
decode_info_to_string = receive_info.decode()
print(decode_info_to_string)
# We will closes the socket
client_socket.close()
