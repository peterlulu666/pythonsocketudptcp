# We will import the socket module
import socket

# It is the same port number as in the client
server_port = 13000
# # We will creates the server’s socket
# # AF_INET indicates that it is using IPv4
# # SOCK_DGRAM indicates that the socket is UDP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# We will bind the port number 13000 to the server’s socket
server_socket.bind(("", server_port))
# We will let the user know that the server is running
print("The server is ready to receive ")
# In the while loop, UDPServer waits for a packet to arrive
while True:
    # When a packet arrives at the server’s socket,
    # the packet’s data is put into the variable info and
    # the packet’s source address is put into the variable client_address
    receive_info, client_address = server_socket.recvfrom(2048)
    # We will count letter a in the receive_info
    decode_info_to_string = receive_info.decode()
    count = 0
    for letter in decode_info_to_string:
        if letter == "a":
            count = count + 1
    count_str = str(count)
    # We will send the count_str into the server’s socket
    # The Internet will then deliver the count_str to this client address
    encode_count_to_bytes = count_str.encode()
    server_socket.sendto(encode_count_to_bytes, client_address)
